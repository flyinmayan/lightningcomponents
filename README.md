## Logging time against records

### Scenario

We're concerned that Sales staff are spending lots of time on leads and opportunities that are not progressing. We'd like to give Sales staff a tool to record how much time they're spending on certain records, so they can make an informed decision to continue working them or not.

### Tasks

- Make a lightning component that can be included on both the lead and opportunity views in Salesforce.
- The component will show the logged time for the current record.
- And it will have an input field for the user to add to the logged time.
- When time is logged, refresh the logged time output.
- If possible, make the component generic enough that it can be used on the layout of any object type without have to change code or configuration.

### Approach

- Use the attached example so you don't need to spend time on look and feel
- Work in a new developer org
- Use SFDX if you can, otherwise the metadata api / tool of choice.
- Work with a production mindset making commits at appropriate points
- Limit yourself to 2 hours max


({
	loadItems : function(component) {
		var itemList = component.get("v.items");
		var action = component.get("c.getItems");
		action.setCallback(this, function(response){
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS"){
				component.set("v.items", response.getReturnValue());
			}else{
				console.log("Error state :  " + state);
			}
		});
		$A.enqueueAction(action);
	},
    validateInput : function(component){
        var validEntry = true;
        
		var nameField = component.find("itemname");
        var itemName = nameField.get("v.value");
        
        if($A.util.isEmpty(itemName)){
            validEntry = false;
            nameField.set("v.errors",[{message:"Item name can't be blank"}]);
        }
        else{
            nameField.set("v.errors",null);
        }
    }
})
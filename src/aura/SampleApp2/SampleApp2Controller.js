({
	handleSampleEvent : function(component, event, helper) {
		component.set("v.setMeOnEventFiring", "Ok I handled it " + event.getParam('message'));
	}
})
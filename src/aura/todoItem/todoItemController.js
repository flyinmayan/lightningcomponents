({
	handleClick : function(component, event, helper) {
		var cmpEvent = $A.get("e.c:todoItemUpdate");
		var todo = component.get('v.todo');
		cmpEvent.setParams({'todo':todo});
		cmpEvent.fire();
	}
})
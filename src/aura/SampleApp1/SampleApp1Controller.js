({
	handleClick : function(component, event) {
		var appEvent = $A.get("e.c:SampleApplicationEvent");
		console.log(appEvent);
		appEvent.setParams({'message':'Clicked! ' + (new Date().toGMTString()) });
		appEvent.fire();
	}
})
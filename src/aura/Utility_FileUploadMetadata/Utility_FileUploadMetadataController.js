({
	doInit : function(component, event, helper) {
		helper.getFileTypeOption(component);
	},
    onFileTypeSelection : function(component, event, helper) {
        debugger;
        var fileTypes = component.find(event.getSource().getLocalId());
        if(fileTypes.length === undefined){
				var fileType = fileTypes;
                if(fileType.get("v.checked")){
                    component.set("v.selectedFile",fileType.get("v.value"));
                    var fileTypeMap = component.get("v.fileTypeMap");
                    var fieldName = fileTypeMap[fileType.get("v.value")];
                    component.set("v.field",fieldName);
                }            
        }else{
            for(var key in fileTypes){
                var fileType = fileTypes[key];
                if(fileType.get("v.checked")){
                    component.set("v.selectedFile",fileType.get("v.value"));
                }
            }
        }
    },
    showUploadFileSection : function(component, event, helper) {
        debugger;
        var selectedFile = component.get("v.selectedFile");
        console.log(component.get("v.selectedFile"));
        if(selectedFile!=undefined && selectedFile!=''){
            component.set("v.title", selectedFile+' Upload');
            var fileTypeMap = component.get("v.fileTypeMap");
            component.set("v.defaultFileName",fileTypeMap[selectedFile].File_Name__c);
            component.set("v.field",fileTypeMap[selectedFile].Field_API_Name__c);            
            component.set("v.title",fileTypeMap[selectedFile].MasterLabel+' Upload');
            component.set("v.selectedMetadataType",fileTypeMap[selectedFile].DeveloperName);
            component.set("v.isfileSelected",true);
        }else{
            $A.util.removeClass(component.find("fileTypeSelectionError"),"slds-hide");
        }
    }   
    
})
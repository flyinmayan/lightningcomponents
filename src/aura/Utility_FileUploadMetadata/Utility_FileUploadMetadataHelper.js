({
    getFileTypeOption : function(cmp) {
        debugger;
        var action = cmp.get('c.getFileTypeOptions');
        action.setParams({
            recordId: cmp.get("v.parentId")
        });
        
        action.setCallback(this,function(response){
            if(response.getState()=='SUCCESS'){
                var returnValue = response.getReturnValue();
                var fileTypeOptionsMap = returnValue.objectData;
                var fileTypeOptions = [];
                //[Shubham Jain 4/16/2017: Logic to order the file type options]
				var otherFileTypeOption;
                for(var key in fileTypeOptionsMap){
                    if(fileTypeOptionsMap[key].MasterLabel == "Other" ){
                        otherFileTypeOption = fileTypeOptionsMap[key];
                    }else{
	                    fileTypeOptions.push(fileTypeOptionsMap[key]);
                    }
                }

                //[Shubham Jain 4/18/2017: Logic to sort the file type options based on File Type]
                fileTypeOptions.sort(function(a,b) {return (a.File_Type__c > b.File_Type__c) ? 1 : ((b.File_Type__c > a.File_Type__c) ? -1 : 0);} ); 
				
                if(otherFileTypeOption!=null){
	                fileTypeOptions.push(otherFileTypeOption);
                }
                console.log(fileTypeOptions);
                cmp.set("v.fileTypeOptions",fileTypeOptions);    
                console.log(fileTypeOptionsMap);
                cmp.set("v.fileTypeMap",fileTypeOptionsMap);
                
            }else{
                console.log("Error:"+response);
            }
        });
        $A.enqueueAction(action);
    }    
})
({
	fetchFiles : function(component) {
        debugger;
		console.log(component.get("v.recordId"));
        var action = component.get("c.fetchOwnFiles");
        action.setParams({"Id":component.get("v.recordId")});
        action.setCallback(this,function(response){
            if(response.getState()=="SUCCESS"){
	            console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.files",response.getReturnValue());
                component.set("v.spinner",false);
            }else{
                
            }
        });
        $A.enqueueAction(action);
	}
})
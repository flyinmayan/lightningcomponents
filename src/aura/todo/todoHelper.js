({
	loadTodos : function(component) {
		var action = component.get("c.getTodos");
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS"){
				component.set("v.todos", response.getReturnValue());
				this.todoTotal(component);
			}else{
				console.log("Failed with state: " + state);
			}
		});
		$A.enqueueAction(action);
	},
	getCtxUser :function(component){
		var action = component.get("c.getUser");
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS"){
				component.set("v.userId", response.getReturnValue());
			}else{
				console.log("Failed to retun user Id with status of : " +state );
			}
		});
		$A.enqueueAction(action);
	},
	validateEntry : function(component){
		var inputField = component.find("todoName");			
		var todoName = inputField.get("v.value");
		
		if($A.util.isEmpty(todoName)){
			inputField.set("v.errors",[{message:"Todo name can't be blank"}]);
			return false;
		}else{
			inputField.set("v.errors",null);
			return true;
		}
	},
	todoTotal : function(component){
		var todos = component.get("v.todos");
		component.get("v.todoTotal");
		component.set("v.todoTotal", todos.length);

	},
	updateTodo : function(component, todo, userId){
		var action = component.get("c.saveTodo");
		var todoList = component.get("v.todos");
		var newTodo = JSON.parse(JSON.stringify(todo));
		action.setParams({
			"todo": newTodo,
			"uId" : userId
		});
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS"){
				console.log(response.getReturnValue());
				var newTodo = JSON.parse(JSON.stringify(response.getReturnValue()));
				todoList.push(newTodo);
				component.set("v.todos", todoList);
				this.todoTotal(component);

			}else{
				console.log(state);
			}
		});
		$A.enqueueAction(action);
	},
	deleteTodo : function(component, todo){
		var action = component.get("c.deleteTodo");
		action.setParams({
			"todo":todo
		});
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state === "SUCCESS"){
				console.log("record successfully deleted");
			}else{
				console.log(state);
			}
		});
		$A.enqueueAction(action);
	}
})
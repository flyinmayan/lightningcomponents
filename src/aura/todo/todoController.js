({
	doInit : function(component,event,helper){
		helper.loadTodos(component);
		helper.getCtxUser(component);
	},
	createTodo : function(component, event, helper) {
		var todoList = component.get("v.todos");
		var todoInputField = component.find("todoName");
		var todoName = todoInputField.get("v.value");
		
		if(event.keyCode === 13){
			if(helper.validateEntry(component)){
				var newTodo = component.get('v.newTodo');
				var userId = component.get('v.userId');
				helper.updateTodo(component, newTodo, userId);
			}
			todoInputField.set("v.value", '');
			helper.todoTotal(component);
		}
	},
	removeTodo : function(component, event, helper){
		var todo = event.getParam('todo');
		var todoList = component.get("v.todos");
		var int = todoList.indexOf(todo);
		if(int > -1){
			todoList.splice(int, 1);
		}
		helper.deleteTodo(component, todo);
		component.set("v.todos", todoList);
		helper.todoTotal(component);
	},
    modalTest : function(component, event, helper){
        var modalCmp = component.find("modalWindow");
        modalCmp.changeVisibility();
        modalCmp.set("v.title","Header Placeholder");
        modalCmp.set("v.body", "Body Placeholder....");
    }
})
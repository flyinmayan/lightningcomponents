({	
	doInit : function(component, event, helper){
		helper.getDateTime(component);
	},
	changeVisiblity : function(component, event, helper){
		var hidden = component.get("v.isVisible");
		if(!hidden){
			component.set("v.isVisible",true);
			helper.getProducts(component);
		}
		else{
			component.set("v.isVisible",false);
		}
	}
})
({
	getDateTime : function(component, event, helper) {
		var today = new Date();
		var hours = today.getHours()+1;
		var dd = "AM";
		if(hours > 12){
			hours = hours - 12;
			dd = "PM";
		}
		if(hours == 0){
			hours = "12"; 
		}
		var minutes = today.getMinutes();
		if(minutes < 10){
			minutes = '0'+ minutes;
		}
		var time = (hours + ':' + minutes);
		var tomorrow = this.addDays(today,1);
		var fDate = ((tomorrow.getFullYear())+'-'+(tomorrow.getMonth()+1)+'-'+(tomorrow.getDate()));
		component.set("v.newShipment", {'sobjectType' : 'Shipment__c',
									    'Delivery_Date__c' : fDate,
									    'PoNumber__c' : '',
										'Delivery_Time__c' : time});
		
	},
	addDays : function(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() + days);
		return result;
	},
	getProducts : function(component, event, helper){
		var shipmentId = component.get("v.shipmentId");
		var action = component.get("c.getProducts");
		action.setParams({
			'shipmentId': shipmentId
		});
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state == "SUCCESS"){
				component.set("v.products", response.getReturnValue());
			}
			else{
				console.log(state);
			}
		});
		$A.enqueueAction(action);
	}
})
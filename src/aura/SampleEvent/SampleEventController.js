({
	handleClick : function(component, event, helper) {
		console.log('Controller handle click');
		var compEvent = component.getEvent("simpleEvent");
		compEvent.setParams({'message':'Clicked! ' +
			(new Date().toGMTString()) });
		compEvent.fire();
	},
	handleSampleEvent : function(component, event, helper){
		console.log('Controller handle sample event');
		component.set('v.setMeOnEventFiring', "Ok I set it " + event.getParam('message'));
	}
})
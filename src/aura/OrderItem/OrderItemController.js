({
	createShipment : function(component, event, helper) {
		var shipmentCmp = component.find("shipmentForm");
		var orderId = component.get("v.order.Id");
		shipmentCmp.set("v.shipmentId", orderId);
		shipmentCmp.changeVisiblity();
	}
})
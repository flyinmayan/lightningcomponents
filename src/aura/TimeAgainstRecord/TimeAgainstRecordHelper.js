({
    getData : function(component) {
        const action = component.get("c.getObjectDetails");
        action.setParams({
            sObjectName : component.get("v.sObjectName"),
            recordId : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state === "SUCCESS"){
                let data = JSON.parse(response.getReturnValue());
                component.set("v.minutes", data.Time_Against_Record__c);
                component.set("v.timeInHoursAndMintues", this.convertTime(data.Time_Against_Record__c));
            }else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    submitTime : function(component, timeInput){
        const action = component.get("c.updateObjectDetails");
        action.setParams({
            sObjectName : component.get("v.sObjectName"),
            recordId : component.get("v.recordId"),
            minutes : timeInput
        });
        action.setCallback(this,function(response){
            const state = response.getState();
            if(state === "SUCCESS"){
                let data = JSON.parse(response.getReturnValue());
                component.set("v.minutes", data.Time_Against_Record__c);
                component.set("v.timeInHoursAndMintues", this.convertTime(data.Time_Against_Record__c));
            }else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    convertTime : function(time){
        let minutes = time % 60;
        let hours = (time - minutes)/60;
        let padHours = hours.toString().padStart(2,'0');
        let padMinutes = minutes.toString().padStart(2,'0');
        return padHours + ':' + padMinutes;
    }
})

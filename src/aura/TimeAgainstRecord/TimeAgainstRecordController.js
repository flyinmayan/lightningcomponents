({
    doInit : function(component, event, helper) {
        helper.getData(component);
    },
    updateTime : function(component,event,helper){
        var timeInput = component.find('timeInput').get("v.value");{}
        if(!$A.util.isEmpty(timeInput)){
            helper.submitTime(component, timeInput);
        }
    }
})

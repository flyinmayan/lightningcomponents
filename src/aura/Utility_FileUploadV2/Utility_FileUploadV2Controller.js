({
    doInit : function(component, event, helper){
        helper.getMetadataAlongWithUploadedFile(component);
    },
    upload : function(component, event, helper) {
        helper.save(component);
    },
    handleFilesChange : function(component, event, helper) {
        helper.handleFilesChange(component);
    },
    deleteFile : function(component, event, helper) {
        helper.deleteFile(component);
    },
    downloadFile : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": location.protocol + '//' + location.host+"/sfc/servlet.shepherd/version/download/"+
            component.get("v.existingFile").Id
        });
        urlEvent.fire();
    }
})
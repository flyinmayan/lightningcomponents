({
    handleFilesChange: function(cmp) {
        $A.util.addClass(cmp.find("fileTypeSelectionError"), "slds-hide");
        //var files = cmp.find("file").get("v.files");
        //var file = files[0];
        var fileInput = cmp.find("file").getElement();
        var files = fileInput.files;
        var file = files[0];
        if(file){
            var fileNameExt = file.name.substring(file.name.lastIndexOf(".")).toLowerCase();
            var acceptTypes = cmp.get("v.accept");
            var maxSizeAllowedInMB = 4.5;
            var definedLimit = cmp.get("v.maxSizeAllowed");
            if (definedLimit && definedLimit < maxSizeAllowedInMB)
                maxSizeAllowedInMB = definedLimit;
            //evaluate a max file size of 4.5mb
            var fileSizeInMB = (file.size / 1000000).toFixed(2);
           
            if (maxSizeAllowedInMB && fileSizeInMB > maxSizeAllowedInMB) {
                //cmp.find("file").set("v.files",[]);
                fileInput.value = '';
                cmp.set("v.fileTypeSelectionErrorMessage", 'File size cannot exceed ' + maxSizeAllowedInMB + ' MB' +
                        '\nSelected file size: ' + fileSizeInMB + ' MB');
                $A.util.removeClass(cmp.find("fileTypeSelectionError"), "slds-hide");
                return;
            } else if (acceptTypes && acceptTypes != '*/*' && !acceptTypes.includes(fileNameExt)) {
                //cmp.find("file").set("v.files",[]);
                fileInput.value = '';
                cmp.set("v.fileTypeSelectionErrorMessage", $A.get("$Label.c.Supported_Files_Error_Message") + acceptTypes);
                $A.util.removeClass(cmp.find("fileTypeSelectionError"), "slds-hide");
                return;
            }
        }
        cmp.set("v.fileName", file.name);
    },
    save: function(cmp) {
        $A.util.addClass(cmp.find("fileTypeSelectionError"), "slds-hide");
        //var files = cmp.find("file").get("v.files");
        
        var fileInput = cmp.find("file").getElement();
        var files = fileInput.files;
        if (files == undefined || files.length == 0) {
            cmp.set("v.fileTypeSelectionErrorMessage","Please select a file");
            $A.util.removeClass(cmp.find("fileTypeSelectionError"), "slds-hide");
            return;
        }        
        var file = files[0];        
        console.log(file);
        var fr = new FileReader();
        var self = this;
        fr.onload = $A.getCallback(function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.upload(cmp, file, fileContents);
        });
        fr.readAsDataURL(file);
    },
    upload: function(cmp, file, fileContents) {
        var fromPos = 0;
        var CHUNK_SIZE = 750000;/* Use a multiple of 4 */
        var toPos = Math.min(fileContents.length, fromPos + CHUNK_SIZE);
        // start with the initial chunk
        this.uploadChunk(cmp, file, fileContents, fromPos, toPos, '');
    },
    uploadChunk: function(cmp, file, fileContents, fromPos, toPos, attachId) {   
        var CHUNK_SIZE = 750000;/* Use a multiple of 4 */
        var chunk = fileContents.substring(fromPos, toPos);
        var fileName = cmp.get("v.fileName");
        var selectedMetadataFileType = cmp.get("v.selectedMetadataFileType");
        if (selectedMetadataFileType != 'Other') {
            var defaultFileName = cmp.get("v.defaultFileName");
            fileName = defaultFileName + fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
        }
        cmp.set("v.fileName", fileName);
        var encodedContents = encodeURIComponent(chunk);
        var self = this;
        self.callServer(cmp, 'c.saveTheVersion', function(returnValue) {
            var uploadedContentVersion = returnValue.objectData['uploadedContentVersion'];
            var uploadedFileId = uploadedContentVersion.Id;
            var newFromPos = toPos;
            var newToPos = Math.min(fileContents.length, newFromPos + CHUNK_SIZE);
            if (newFromPos < newToPos) {
                self.uploadChunk(cmp, file, fileContents, newFromPos, newToPos, uploadedFileId);
            } else {  
                if(selectedMetadataFileType != 'Other'){
                    self.callServer(cmp, 'c.updateSObjectFileFields', function(returnValue2) {
                        cmp.set("v.showUploadSuccessMessage", true);
                        cmp.set("v.uploadedFileId", uploadedFileId);
                        cmp.set("v.existingFile", uploadedContentVersion);
                        cmp.set("v.showExistingFile", true);
                        cmp.set("v.showUploadFile", false);
                        console.log("returnValue for updateSObjectFileFields " + JSON.stringify(returnValue2));
                    }, {
                        recordId: cmp.get('v.parentId'),
                        fileld: uploadedFileId, 
                        fieldName: cmp.get('v.field')
                    }, false);
                }
                else{
                    cmp.set("v.showUploadSuccessMessage", true);
                    cmp.set("v.uploadedFileId", uploadedFileId);
                    cmp.set("v.existingFile", uploadedContentVersion);
                    cmp.set("v.showExistingFile", true);
                    cmp.set("v.showUploadFile", false);
                }
            }
        }, {
            parentId: cmp.get('v.parentId'),
            fileName: fileName,
            base64Data: encodedContents,
            contentType: file.type,
            fileId: attachId,
            fieldName: cmp.get('v.field')
        }, false);
    },
    deleteFile: function(cmp) {
        var attachId = cmp.get("v.uploadedFileId");
        cmp.set("v.spinner", true);
        this.callServer(cmp, 'c.deleteUploadedFile', function(returnValue) {            
            console.log('deleteUploadedFile ');
            console.log(returnValue);
            var fileInput = cmp.find("file").getElement();
            fileInput.value = '';
            cmp.set("v.fileName", '');
            cmp.set("v.showUploadSuccessMessage", false);
            cmp.set("v.showExistingFile", false);
            cmp.set("v.showUploadFile", true);
            
            var selectedMetadataFileType = cmp.get("v.selectedMetadataFileType");
            if (selectedMetadataFileType != 'Other') {
                this.callServer(cmp, 'c.updateSObjectFileFields', function(returnValue2) {
                    console.log('updateSObjectFileFields ');
                    console.log(returnValue2);
                    cmp.set("v.uploadedFileId",'');
                    cmp.set("v.existingFile",{});
                }, {
                    recordId: cmp.get('v.parentId'),
                    fileld: '',
                    fieldName: cmp.get('v.field')
                }, false);
            }
            
        }, {
            fileId: attachId
        }, false);
    },
    getMetadataAlongWithUploadedFile : function(cmp) {
        var selectedMetadataType = cmp.get("v.selectedMetadataType");
        var parentId = cmp.get("v.parentId");
        if(parentId){
            this.callServer(cmp, 'c.getMetadataAlongWithUploadedFile',
                            function(returnValue) {
                                var selectedMetadataTypeObj = returnValue.objectData;
                                console.log(selectedMetadataTypeObj);
                                if(selectedMetadataTypeObj && selectedMetadataTypeObj.fileTypeMetadata){
                                    cmp.set("v.accept",selectedMetadataTypeObj.fileTypeMetadata.Allowed_File_Types__c.toLowerCase());
                                    cmp.set("v.defaultFileName",selectedMetadataTypeObj.fileTypeMetadata.File_Name__c);
                                    cmp.set("v.field",selectedMetadataTypeObj.fileTypeMetadata.Field_API_Name__c);
                                    cmp.set("v.selectedMetadataTypeLabel",selectedMetadataTypeObj.fileTypeMetadata.MasterLabel);
                                    cmp.set("v.selectedMetadataFileType",selectedMetadataTypeObj.fileTypeMetadata.File_Type__c);
                                    
                                    if(selectedMetadataTypeObj.existingVersionId!=undefined && selectedMetadataTypeObj.existingVersionId!=''){
                                        cmp.set("v.uploadedFileId",selectedMetadataTypeObj.existingVersionId);                                    
                                        if(selectedMetadataTypeObj.existingFileVersion){
                                            var existingFileVersion = selectedMetadataTypeObj.existingFileVersion;
                                            console.log(existingFileVersion); 
                                            cmp.set("v.existingFile", existingFileVersion);
                                            console.log(cmp.get("v.existingFile"));
                                            cmp.set("v.showExistingFile", true);
                                            cmp.set("v.showUploadFile", false);
											// Changes implemented as a part of Add dependent section on I20/DS2019 forms
                                            cmp.set("v.showUploadSuccessMessage", false);
                                            // Change Ends
                                        }                                    
                                    }// Changes implemented as a part of Add dependent section on I20/DS2019 forms   
                                    else if(selectedMetadataTypeObj.existingVersionId == undefined || selectedMetadataTypeObj.existingVersionId == ''){
                                        cmp.set("v.existingFile", {});
                                        cmp.set("v.uploadedFileId",'');
                                        cmp.set("v.showExistingFile", false);
                                        cmp.set("v.showUploadFile", true);
                                        cmp.set("v.fileName", '');
                                        cmp.set("v.showUploadSuccessMessage", false);
                                    } // Chnage Ends
                                }                           
                            },{
                                selectedMetadataType: selectedMetadataType,
                                recordID: parentId,
                            }, false);   
        }
    },
    //Generic Method to call server methods. Accepts component, methodName, callback function, parameters to be passed to server method and cacheable as input
    callServer: function(cmp, method, callback, params, cacheable) {
        cmp.set("v.spinner", true);
        var action = cmp.get(method);
        if (params) {
            action.setParams(params);
            console.log(params);
        }
        if (cacheable) {
            action.setStorable();
        }
        action.setCallback(this, function(response) {
            cmp.set("v.spinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                // pass returned value to callback function
                var lightningServerResponse = response.getReturnValue();
                if (lightningServerResponse.isSuccessful) {
                    callback.call(this, lightningServerResponse);            
                } else {
                    cmp.set("v.showError", true);
                    cmp.set("v.errorMessage", lightningServerResponse.errorMessage);
                    cmp.set("v.showExistingFile", false);
                    cmp.set("v.showUploadFile", false);
                }
            } else if (state === "ERROR") {
                // generic error handler
                var errors = response.getError();
                var errorMsg = '';
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        errorMsg = errors[0].message;
                        console.log(errorMsg);
                    }                    
                } else {
                    errorMsg = "Unknown Error";                    
                }
                cmp.set("v.showExistingFile", false);
                cmp.set("v.showUploadFile", false);
                cmp.set("v.showError", true);
                cmp.set("v.errorMessage", "Failed " + errorMsg);
            }
        });
        $A.enqueueAction(action);
    }
})
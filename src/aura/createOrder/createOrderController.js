({
	doInit : function(component, event, helper) {
		var action = component.get("c.getShipments");
		action.setCallback(this,function(response){
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS"){
				component.set("v.shipments", response.getReturnValue());
			}
			else{
				console.log(state);
			}
		});
		$A.enqueueAction(action);
	}
})
({
	doInit : function(component, event, helper) {
        //Call d3 methods here:
        
		var tooltip = d3.select('body')            
		  .append('div')                             
		  .attr('class', 'tooltip');                 

		tooltip.append('div')                        
		  .attr('class', 'label');                   

		tooltip.append('div')                        
		  .attr('class', 'count');                   

		tooltip.append('div')                        
		  .attr('class', 'percent');                 

		var dataset = [
				  {category: "Arts, Entertainment, Media, & Sports", value: 2},
					{category: "Business, Innovation, Science, and Tech", value: 2},
					{category: "Community Service & Philanthropy", value: 2},
					  {category: "Public Service", value: 2},   
					  {category: "Unsung Hero", value: 2},
					  {category: "Legacy", value: 2},
		];

		var width = 400;
		var height = 400;
		var radius = Math.min(width, height) / 3;

		var color = d3.scaleOrdinal(d3.schemeCategory10);

		var svg = d3.select('body')
				  .append('svg')
				  .attr('width', width)
				  .attr('height', height)
				  .append('g')
				  .attr('transform', 'translate(' + (width / 2) + 
					',' + (height / 2) + ')');
				
		var donutWidth = 55;

		var arc = d3.arc()
			.innerRadius(radius - donutWidth)
			.outerRadius(radius);
		
		var arcOver = d3.arc()
			.innerRadius((radius - donutWidth)*1.2)
			.outerRadius(radius * 1.2);

		var pie = d3.pie()
				  .value(function(d) { return d.value; })
				  .sort(null);

		var legendRectSize = 18;
		var legendSpacing = 4;
				
		var path = svg.selectAll('path')
				  .data(pie(dataset))
				  .enter()
				  .append('path')
				  .attr('d', arc)
				  .attr('fill', function(d, i) { 
					return color(d.data.category);
				  });
				
		path.on('mouseover', function(d) {
			d3.select(this).transition()
			               .duration(500)
			               .attr("d", arcOver);
			var total = d3.sum(dataset.map(function(d) {
						return d.value;
			}));
			var percent = Math.round(1000 * d.data.value / total) / 10;
				tooltip.select('.label').html(d.data.category);
				tooltip.select('.count').html(d.data.value);
				tooltip.select('.percent').html(percent + '%');
				tooltip.style('display', 'block');
		});
							
		path.on('mouseout', function() {
			d3.select(this).transition()
			               .duration(500)
			               .attr("d", arc)
			tooltip.style('display', 'none');
            });
				 
	}
})
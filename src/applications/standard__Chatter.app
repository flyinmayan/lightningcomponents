<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>Communication__c</tab>
    <tab>Camping_Item__c</tab>
    <tab>todo__c</tab>
    <tab>Shipment__c</tab>
    <tab>Product__c</tab>
    <tab>Suggestion__c</tab>
    <tab>Property__c</tab>
</CustomApplication>

public with sharing class orderController {
	@AuraEnabled
	public static List<Shipment__c> getShipments(){
		return [SELECT Id, Name, PoNumber__c FROM Shipment__c ORDER BY CreatedDate asc];
	}
}
public class timeAgainstRecordsController {
	@AuraEnabled 
    public static String getObjectDetails(String sObjectName, String recordId){
        try{
            String strQuery = 'SELECT Id, Time_Against_Record__c FROM ' + SObjectName + ' WHERE Id = \'' + recordId + '\' LIMIT 1';
            SObject ctxObject = Database.query(strQuery);
            return JSON.serialize(ctxObject);
        }catch(Exception e){
            return e.getMessage();
        }
    }

    @AuraEnabled
    public static String updateObjectDetails(String sObjectName, String recordId, Integer minutes){
        try{
            String strQuery = 'SELECT Time_Against_Record__c FROM ' + SObjectName + ' WHERE Id = \'' + recordId + '\' LIMIT 1';
            Integer previousTime = Integer.valueOf(
                Database.query(strQuery)[0].get('Time_Against_Record__c'));
            Integer newTime = Integer.valueOf(minutes);
            if(previousTime != null){
                newTime = previousTime + newTime;
            }
            System.debug(newTime);
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.SobjectType objectType = gd.get(sObjectName);
            SObject myObj = objectType.newSObject();
            myObj.Id = recordId;
            myObj.put('Time_Against_Record__c', newTime);
            update myObj;
            return JSON.serialize(myObj);
        }catch(Exception e){
            return e.getMessage();
        }
    }
}

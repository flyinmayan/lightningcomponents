public with sharing class CampingController {
	@AuraEnabled
	public static List<Camping_Item__c> getItems(){
		List<Camping_Item__c> cItems = [SELECT Id, Name, Price__c, Quantity__c, Packed__c FROM Camping_Item__c];
		return cItems;
	}
}
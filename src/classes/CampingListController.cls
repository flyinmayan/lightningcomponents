public class CampingListController {
	@AuraEnabled
	public static List<Camping_Item__c> getItems(){
		List<Camping_Item__c> cItems = [SELECT Id, Name, Price__c, Quantity__c, Packed__c FROM Camping_Item__c ORDER BY CreatedDate asc];
		return cItems;
	}
    @AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c item){
        upsert item;
        return item;
    }
}
public with sharing class todoController {
	public static Id ctxtUser = UserInfo.getUserId() ;

	@AuraEnabled
	public static List<todo__c> getTodos(){
		List<todo__c> todos = [SELECT Id, Name, User__c FROM todo__c  WHERE User__c =: ctxtUser ORDER BY CreatedDate asc];
		return todos;
	}

	@AuraEnabled
	public static todo__c saveTodo(todo__c todo, Id uId){
		todo.User__c = uId;
		try{
			upsert todo;
		}catch(Exception e){
			System.debug(LoggingLevel.ERROR, e.getMessage());
		}
		return todo;
	}
	@AuraEnabled
	public static todo__c deleteTodo(todo__c todo){
		System.debug(todo);
		delete todo;
		return null;
	}

	@AuraEnabled
	public static Id getUser(){
		return ctxtUser;
	}
}
public with sharing class shipmentController {
	@AuraEnabled
	public static List<Shipment__c> getShipments(){
		return [SELECT Id, Name FROM Shipment__c];
	}

	@AuraEnabled
	public static List<Product__c> getProducts(Id shipmentId){
		List<Shipment__c> shipments = [SELECT Id,(SELECT Id, Name FROM Products__r)FROM Shipment__c WHERE Id =: shipmentId];
		List<Product__c> products = new List<Product__c>();
		for(Shipment__c s: shipments){
			for(Product__c p: s.Products__r){
				products.add(p);
			}
		}
		return products;
	}
}